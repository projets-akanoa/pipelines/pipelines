use std::io::Read;
use std::net::TcpStream;
use crossbeam_channel::{Receiver, Sender, unbounded};
use crate::data::Message;
use crate::errors;


pub struct TCPInput {
    peer : String,
    stream : TcpStream,
    sender : Option<Sender<Message>>,
}

impl TCPInput {
    pub fn new(addr : &str) -> Result<Self, errors::ConnectionError> {

        println!("{}", addr);

        let connection = TcpStream::connect(addr);

        let peer = addr.to_string();

        if connection.is_err() {
            return Err(errors::ConnectionError::TcpConnectionError {peer})
        }

        Ok(TCPInput {stream: connection.unwrap(), peer, sender: None})
    }

    pub fn run(&mut self) -> std::io::Result<()> {

        let mut data = [0 as u8; 1024];
        loop {
            match self.stream.read_exact(&mut data) {
                Ok(_) => {
                    //println!("{:?}", &data.iter().map(|&c| c as char).collect::<String>());


                    match &self.sender {
                        Some(sender) => {

                            if let Err(_e) = sender.send(Message {buf: data.to_vec()}) {
                                continue;
                            }
                        },
                        None => {}
                    };
                },
                Err(e) => {
                    println!("nop");
                    break
                }
            };
        }
        Ok(())
    }

    pub fn set_sender(&mut self, sender: Sender<Message>) -> &Self {
        self.sender = Some(sender);
        self
    }

    pub fn create_channels() -> (Sender<Message>, Receiver<Message>) {
        unbounded()
    }
}
