use thiserror::Error;

#[derive(Error, Debug)]
pub enum ConnectionError {
    #[error("Unable to connect to TCP server")]
    TcpConnectionError { peer: String },

    #[error("Unable to bind to local socket")]
    UdpBindingError {},

    #[error("Unable to push data to channel")]
    SendingError {}
}

